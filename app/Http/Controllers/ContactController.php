<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transformer\ContactTransformer;
use App\Service\Contacts\ContactService;
use App\Exceptions\Services\ServiceProcessingException;

/**
 * Class ContactController
 * @package App\Http\Controllers;
 */
class ContactController extends BaseController
{
    /**
     * @var \App\Service\Contacts\ContactService
     */
    protected $contactService;

    /**
     * ContactController constructor.
     * @param \App\Service\Contacts\ContactService $contactService
     */
    public function __construct(ContactService $contactService)
    {
        $this->contactService = $contactService;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return mixed
     */
    public function create(Request $request)
    {
        try {
            $loggedUser = $this->loggedUser();
            $contact = $this->contactService->createContact($request->all(), $loggedUser->id);

            return $this->item($contact, new ContactTransformer());
        } catch (ServiceProcessingException $error) {
            $this->throwErrorStore($error->getMessage());
        }
    }

    /**
     * @param int $contactId
     * @return mixed
     */
    public function delete($contactId)
    {
        try {
            $loggedUser = $this->loggedUser();
            $this->contactService->delete($contactId, $loggedUser->id);
            return $this->array([
                'data' => [
                    'message' => 'Contact has been delete successfully'
                ]
            ]);
        } catch (ServiceProcessingException $error) {
            $this->throwErrorDelete($error->getMessage());
        }
    }

    /**
     * @param int $contactId
     * @return mixed
     */
    public function getById($contactId)
    {
        try {
            $contacts = $this->contactService->getContactById($contactId);
            return $this->item($contacts, new ContactTransformer());
        }catch (ServiceProcessingException $error) {
            $this->throwErrorNotFound($error->getMessage());
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param int $contactId
     * @return mixed
     * @throws \App\Exceptions\Services\ServiceProcessingException
     */
    public function update(Request $request, $contactId)
    {
        try {
            $this->contactService->update($request->all(), $contactId);

            return $this->array([
                'data' => [
                    'message' => 'Contact has been update successfully'
                ]
            ]);
        } catch (Exception $error) {
            $this->throwErrorUpdate($error->getMessage());
        }
    }

    /**
     * @param int $userId
     * @return mixed
     */
    public function getContactByUserId($userId)
    {
        try {
            $contacts = $this->contactService->getContactByUserId($userId);
            return $this->collection($contacts, new ContactTransformer());
        }catch (ServiceProcessingException $error) {
            $this->throwErrorNotFound($error->getMessage());
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return mixed
     */
    public function filterContact(Request $request)
    {
        $contactName = $request->get('name');
        try {
            $loggedUser = $this->loggedUser();
            $contacts = $this->contactService->getContactByNameAndUserId($contactName, $loggedUser->id);
            return $this->collection($contacts, new ContactTransformer());
        }catch (ServiceProcessingException $error) {
            $this->throwErrorNotFound($error->getMessage());
        }
    }
}
