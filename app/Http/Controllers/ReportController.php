<?php

namespace App\Http\Controllers;

use Exception;
use App\Service\Reports\ReportService;
use App\Transformer\ContactTransformer;

class ReportController extends BaseController
{
    /**
     * @var \App\Service\Reports\ReportService
     */
    protected $reportService;

    /**
     * ReportController constructor.
     * @param \App\Service\Reports\ReportService $reportService
     */
    public function __construct(ReportService $reportService)
    {
        $this->reportService = $reportService;
    }

    /**
     * @return mixed
     */
    public function generateContactWithPhoneReport()
    {
        try {
            $loggedUser = $this->loggedUser();
            $report = $this->reportService->generateContactWithPhoneReport($loggedUser->id);
            return $this->collection($report, new ContactTransformer());
        } catch (Exception $error) {
            $this->throwErrorBadRequest($error->getMessage());
        }
    }

    /**
     * @return mixed
     */
    public function generateContactWithoutPhoneReport()
    {
        try {
            $loggedUser = $this->loggedUser();
            $report = $this->reportService->generateContactWithoutPhoneReport($loggedUser->id);
            return $this->collection($report, new ContactTransformer());
        } catch (Exception $error) {
            $this->throwErrorBadRequest($error->getMessage());
        }
    }
}
