<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service\Users\UserService;
use App\Transformer\UserTransformer;
use App\Exceptions\Services\ServiceProcessingException;

class UserController extends BaseController
{
    protected $userService;

    /**
     * UserController constructor.
     * @param \App\Service\Users\UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @param int $userId
     * @return mixed
     */
    public function getUser($userId)
    {
        try {
            return $this->array([
                'data' => $this->userService->getUserById($userId)
            ]);
        } catch (ServiceProcessingException $error) {
            $this->throwErrorNotFound($error->getMessage());
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param int $userId
     * @return mixed
     */
    public function updateUser(Request $request, $userId)
    {
        try {
            $inputs = $request->all();
            $user   = $this->userService->updateUser($inputs, $userId);
            return $this->item($user, new UserTransformer());
        } catch (ServiceProcessingException $error) {
            $this->throwErrorUpdate($error->getMessage());
        }
    }

    /**
     * @return mixed
     */
    public function getUserByToken()
    {
        return $this->item($this->loggedUser(), new UserTransformer());
    }
}
