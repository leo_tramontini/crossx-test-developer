<?php


namespace App\Http\Controllers;

use Auth;
use Dingo\Api\Routing\Helpers;
use App\Http\Controllers\Traits\MakesResponses;

class BaseController extends Controller
{
    use Helpers,
        MakesResponses;

    /**
     * Captura o usuário logado
     *
     * @return \App\Models\User
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     */
    public function loggedUser()
    {
        if (! Auth::check()) {
            $this->response->errorForbidden('Unauthorized user');
        }

        return Auth::user();
    }

}
