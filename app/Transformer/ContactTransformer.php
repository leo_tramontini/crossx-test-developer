<?php

namespace App\Transformer;

use App\Models\Contact;
use League\Fractal\TransformerAbstract;

class ContactTransformer extends TransformerAbstract
{
    /**
     * @param \App\Models\Contact $contact
     * @return array
     */
    public function transform(Contact $contact)
    {
        return [
            'id'        => $contact->id,
            'name'      => $contact->name,
            'phones'    => $contact->phones->pluck('phone')
        ];
    }
}
