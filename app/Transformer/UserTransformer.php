<?php


namespace App\Transformer;

use App\Models\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    public function transform(User $user)
    {
        return [
            'id' => $user->id,
            'name' => $user->name,
            'image_path' => is_null($user->image_path) ? '' : asset("storage/images/$user->image_path")
        ];
    }
}
