<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Phone
 * @package App\Models
 */
class Phone extends Model
{
    use SoftDeletes;
    /**
     * @var array
     */
    protected $fillable = [
        'phone',
        'contact_id'
    ];
}
