<?php


namespace App\Service\Reports;

use DB;
use App\Repositories\ContactRepository;
use App\Service\Contacts\ContactService;

class ReportService
{
    /**
     * @var \App\Repositories\ContactRepository
     */
    protected $contactRepository;

    /**
     * ReportService constructor.
     * @param \App\Service\Contacts\ContactService $contactRepository
     */
    public function __construct(ContactRepository $contactRepository)
    {
        $this->contactRepository = $contactRepository;
    }

    /**
     * @param int $userId
     * @return \Illuminate\Support\Collection
     */
    public function generateContactWithPhoneReport($userId)
    {
        return $this->contactRepository->findWhere(['user_id' => $userId])
             ->filter(function ($contact) {
                 if ($contact->phones->count() > 0) {
                     return $contact;
                 }
             });
    }

    /**
     * @param int $userId
     * @return \Illuminate\Support\Collection
     */
    public function generateContactWithoutPhoneReport($userId)
    {
        return $this->contactRepository->findWhere(['user_id' => $userId])
             ->filter(function ($contact) {
                 if ($contact->phones->count() == 0) {
                     return $contact;
                 }
             });
    }
}
