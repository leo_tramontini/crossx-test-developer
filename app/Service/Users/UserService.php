<?php


namespace App\Service\Users;

use Exception;
use App\Repositories\UserRepository;
use App\Exceptions\Services\ServiceProcessingException;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;

/**
 * Class UserService
 * @package App\Service\Users
 */
class UserService
{
    /**
     * @var \App\Repositories\UserRepository
     */
    protected $userRepository;

    /**
     * UserService constructor.
     * @param \App\Repositories\UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param array $userData
     * @param int $userId
     * @return mixed
     * @throws \App\Exceptions\Services\ServiceProcessingException
     */
    public function updateUser($userData, $userId)
    {
        try {
            if (!is_null($userData['image'])) {
                Arr::set($userData,
                    'image_path',
                    $this->storeImage($userData['image'], $userId)
                );
            }

            return $this->userRepository->update($userData, $userId);
        } catch (Exception $error) {
            throw new ServiceProcessingException($error->getMessage(), $error->getCode());
        }
    }

    /**
     * @param int $userId
     * @return mixed
     * @throws \App\Exceptions\Services\ServiceProcessingException
     */
    public function getUserById($userId)
    {
        try {
            return $this->userRepository->find($userId);
        } catch (Exception $error) {
            throw new ServiceProcessingException($error->getMessage(), $error->getCode());
        }
    }

    /**
     * @param \Illuminate\Http\UploadedFile $image
     * @param int $userId
     * @return string
     */
    public function storeImage($image, $userId)
    {
        $extension  = $image->extension();
        $path       = 'public/images';
        $nameImage  = md5(microtime()) . '.' . $extension;

        $image->storeAs($path, $nameImage, 'local');

        return $nameImage;
    }
}
