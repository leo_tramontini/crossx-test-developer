<?php

namespace App\Service\Contacts;

use Exception;
use App\Repositories\PhoneRepository;
use App\Repositories\ContactRepository;
use App\Exceptions\Services\ServiceProcessingException;
use Illuminate\Support\Arr;

/**
 * Class ContactService
 * @package App\Service\Contacts
 */
class ContactService
{
    /**
     * @var \App\Repositories\ContactRepository
     */
    protected $contactRepository;

    /**
     * @var \App\Repositories\PhoneRepository
     */
    protected $phoneRepository;

    /**
     * ContactService constructor.
     * @param \App\Repositories\ContactRepository $contactRepository
     */
    public function __construct(ContactRepository $contactRepository, PhoneRepository $phoneRepository)
    {
        $this->phoneRepository      = $phoneRepository;
        $this->contactRepository    = $contactRepository;
    }

    /**
     * @param array $inputs
     * @return mixed
     * @throws \App\Exceptions\Services\ServiceProcessingException
     */
    public function createContact($inputs, $userId)
    {
        try {
            $contact = $this->contactRepository->create(array_merge($inputs, ['user_id' => $userId]));

            if (isset($inputs['phones'])) {
                $this->createPhones($inputs['phones'], $contact->id);
            }

        } catch (Exception $error) {
            throw new ServiceProcessingException($error->getMessage(), $error->getCode());
        }

        return $contact;
    }

    /**
     * @param array $phones
     * @param int $contactId
     * @return array
     * @throws \App\Exceptions\Services\ServiceProcessingException
     */
    public function createPhones($phones, $contactId)
    {
        try {
            foreach ($phones as $phone) {
                $phones[] = $this->phoneRepository->create([
                    'phone'         => $phone,
                    'contact_id'    => $contactId
                ]);
            }
        } catch (Exception $error) {
            throw new ServiceProcessingException($error->getMessage());
        }

        return $phones;
    }

    /**
     * @param int $contactId
     * @param int $userId
     * @return int
     * @throws \App\Exceptions\Services\ServiceProcessingException
     */
    public function delete($contactId, $userId)
    {
        try {
            $this->contactRepository->deleteWhere([
                'id'        => $contactId,
                'user_id'   => $userId
            ]);
            return $this->phoneRepository->deleteWhere([
                'contact_id' => $contactId
            ]);
        } catch (Exception $error) {
            throw new ServiceProcessingException($error->getMessage());
        }
    }

    /**
     * @param array $filter
     * @return mixed
     * @throws \App\Exceptions\Services\ServiceProcessingException
     */
    public function filterContact($filter)
    {
        try {
            return $this->contactRepository->findWhere($filter);
        } catch (Exception $error) {
            throw new ServiceProcessingException($error->getMessage(), $error->getCode());
        }
    }

    /**
     * @param string $contactName
     * @param int $userId
     * @return mixed
     * @throws \App\Exceptions\Services\ServiceProcessingException
     */
    public function getContactByNameAndUserId($contactName, $userId)
    {
        try {
            if (empty($contactName)) {
                return $this->getContactByUserId($userId);
            }

            return $this->contactRepository->findWhere([
                'name'      => $contactName,
                'user_id'   => $userId
            ]);
        } catch (Exception $error) {
            throw new ServiceProcessingException($error->getMessage(), $error->getCode());
        }
    }

    /**
     * @param int $userId
     * @return mixed
     * @throws \App\Exceptions\Services\ServiceProcessingException
     */
    public function getContactByUserId($userId)
    {
        try {
            return $this->filterContact([
                'user_id' => $userId
            ]);
        } catch (Exception $error) {
            throw new ServiceProcessingException($error->getMessage(), $error->getCode());
        }
    }
    /**
     * @param int $contactId
     * @return mixed
     * @throws \App\Exceptions\Services\ServiceProcessingException
     */
    public function getContactById($contactId)
    {
        try {
            return $this->contactRepository->find($contactId);
        } catch (Exception $error) {
            throw new ServiceProcessingException($error->getMessage(), $error->getCode());
        }
    }

    /**
     * @param array $inputs
     * @param int $contactId
     * @return mixed
     * @throws \App\Exceptions\Services\ServiceProcessingException
     */
    public function update($inputs, $contactId)
    {
        try {
            $this->phoneRepository->deleteWhere([
                'contact_id' => $contactId
            ]);

            $this->createPhones(Arr::get($inputs, 'phones'), $contactId);

            return $this->contactRepository->update($inputs, $contactId);
        }catch (Exception $error) {
            throw new ServiceProcessingException($error->getMessage(), $error->getCode());
        }
    }
}
