<?php

namespace App\Repositories;

use App\Models\Phone;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class PhoneRepository
 * @package App\Repositories
 */
class PhoneRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Phone::class;
    }
}
