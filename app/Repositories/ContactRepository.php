<?php

namespace App\Repositories;

use App\Models\Contact;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class ContactRepository
 * @package App\Repositories
 */
class ContactRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Contact::class;
    }
}
