# Teste: Leonardo Gonçalves Tramontini

### Prazo: 03/03/2020
##### *Antes de iniciar faça o fork do projeto e informe o prazo para a conclusão no primeiro commit. Ao finalizar abrir uma PR para Rodrigo Priolo(rodrigo_priolo)

## Projeto
O projeto consiste em criar uma agenda de contatos parecida com a do android / IOS.
Deve possuir um CRUD de pessoas com soft delete, ou seja, nenhum dado deve ser excluído do banco.

## Desafio
Ao clicar para incluir ou editar o contato abrir o modal do contato e possibilitar:
	- Alterar nome da pessoa.
	- Incluir 1 ou mais telefones.
	- Excluir 1 ou mais telefones.
	- É possível que o contato tenha apenas o nome sem nenhum telefone.

Alterar foto do perfil do usuário logado:
	- IMPORTANTE: A imagem deve ser carregada, sem a necessidade de recarregar a página (usar ajax).

Na tela de contatos, possibilitar pesquisar por nome.

### Relatórios
	- Exibir todas as pessoas, que não possuem telefone cadastrado, ou seja, possuem apenas o nome, mas nenhum contato.
	- Exibir todas as pessoas que possuem telefone cadastrado.

### Requisitos
- Login (pode ser com um usuário gerado previamente);
- Todos os formulários (Inclusão, Edição) devem abrir num modal.
- Utilizar algum framework PHP (Laravel 5+, Yii2, Zend2, etc);
- A cada commit, colocar um descritivo da funcionalidade concluída / ajustada.

### Diferencial
- Utilizar máscaras nos campos.
- Questionar se realmente deseja excluir o registro.
- Exibir mensagens (flashes) na tela após cada ação. Ex: Contato adicionado com sucesso.
- Utilizar migrations.
- Utilizar dependêcias com o composer.
- Utilizar algum framework JS (Rect, Angular +2, Vue).
- Utilizar Docker.
- Utilizar Testes unitários.

### Template
Incluímos um template no repositório, não é obrigado utilizá-lo.


### Não esqueça
Não esqueça de editar este readme no final, nos dizendo como fazemos para rodar seu projeto em nossa máquina, e qual usuário devemos utilizar para logar no sistema

##### *Em caso de dúvidas, sinta-se à vontade para entrar em contato com [rodrigo.priolo@avecbrasil.com.br](rodrigo.priolo@avecbrasil.com.br).

##Pré instalação

Para execução do projeto deve ser instalado dois softwares:

- [Docker](https://docs.docker.com/).
- [Docker Compose](https://docs.docker.com/compose/install/).

##Configuração do ambiente

Após o clone do projeto e ter instalado o Docker e Docker Composer, devemos entrar no diretório crossx-test-developer/docker para executar o seguintes comandos:

`$ sudo docker-compose build`

`$ sudo docker-compose up -d`

Ao executar estes comando será construidos os containers: postgres, application e pgadmin4.

Devemos definiar o host que usaremos para acessar a aplicação, se o ambiente for linux basta executar o comando que segue:

`$ sudo nano /etc/hosts` e adicionar a linha `127.0.0.1 contactlist.local`.

Em seguida deve ser executado o comando:

`$ sudo docker exec -ti application bash`

O acesso dentro do container deve ser como usuário docker pois o mesmo possui a mesmas permissões do usuário da máquina.

`$ su docker`

Entrando no diretório da aplicação 

`$ cd crossx-test-developer`

Devemos instalar as dependência tanto do php quanto do javacript, basta exectuar os comandos:

`$ composer install`

`$ yarn install` ou `$ npm install`

Para compilar o javacript basta executar:

`$ yarn run dev` ou `$ npm run dev`

Em seguida devemos copiar o .env.example e colar em um arquivo chamado .env
 
Após é necessário executar alguns comandos:

O primeiro comando serve para gerarmos a chave que vai gerar os tokens do jwt e o seguinte para gerar um link acessível para o storage do projeto onde as imagens serão gravadas. 

`php artisan jwt:secret && php artisan storage:link` 

Posteriormente devemos criar nossa base de dados em nosso pgadmin para que possamos rodar as migrations e a seed.

Em um navegador basta acessar `http://localhost:5050/`, abrirá a tela de login do pgadmin onde o email é `pgadmin4@pgadmin.org` e a senha é `admin`.

Ao acessar no lado esquerdo temos uma opção chamada `Servers`, clicando com o botão direito do mouse nele, selecionaremos a opção `Create` -> `Server...`.

Sendo assim irá abrir uma tela e devemos preencher a mesma com algumas informações:

Na aba `General` colocaremos um nome no campo `Name`, podemos colocar por exemplo: `aplicacao` e clicar em `Save`.

Na aba `Connection` devemos inserir na opção `Hosts name/address` a palavra `postgres` pois usaremos o container criado anteriormente.
Na mesma aba nos campos `Username` e `Password` vamos colocar `postgres` também e clicar em `Save`.

Em seguida vamos clicar em cima de `Database` com o botao direito do mouse e vamos na opção `Create` -> `Database ...`, deve ser colocado no campo `Database` a palavra `contactlist`.

Finalizamos a configuração no pgadmin, sendo assim podemos voltar em nosso container e executar o comando `php artisan migrate --seed`.

Após nosso aplicação estará disponível para uso basta acessar: `http://contactlist.local` e utilizar o sistema.

Usuário para acesso no sistem é: avec@test.com.br Senha: avec123
