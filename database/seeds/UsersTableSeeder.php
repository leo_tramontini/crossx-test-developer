<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'      => 'Teste Avec',
            'email'     => 'avec@test.com.br',
            'password'  => bcrypt('avec123')
        ]);
    }
}
