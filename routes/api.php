<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->post('login', 'App\Http\Controllers\AuthController@login');
    $api->post('logout', 'App\Http\Controllers\AuthController@logout');
    $api->post('refresh', 'App\Http\Controllers\AuthController@refresh');
    $api->post('me', 'App\Http\Controllers\AuthController@me');

    $api->group(['middleware' => 'auth.jwt'], function ($api) {

        $api->get('user/token', 'App\Http\Controllers\UserController@getUserByToken');
        $api->get('user/{userId}', 'App\Http\Controllers\UserController@getUser')
            ->where('userId', '[0-9]+');

        $api->post('user/{userId}', 'App\Http\Controllers\UserController@updateUser')
            ->where('userId', '[0-9]+');

        $api->post('contact', 'App\Http\Controllers\ContactController@create');
        $api->put('contact/{contactId}', 'App\Http\Controllers\ContactController@update');
        $api->delete('contact/{contactId}', 'App\Http\Controllers\ContactController@delete')
            ->where('contactId', '[0-9]+');

        $api->get('contact/{contactId}', 'App\Http\Controllers\ContactController@getById')
            ->where('contactId', '[0-9]+');

        $api->get('contact/user/{userId}', 'App\Http\Controllers\ContactController@getContactByUserId')
            ->where('contactId', '[0-9]+');

        $api->get('contact/filter', 'App\Http\Controllers\ContactController@filterContact');

        $api->get('report/contact-with-phones', 'App\Http\Controllers\ReportController@generateContactWithPhoneReport');
        $api->get('report/contact-without-phones', 'App\Http\Controllers\ReportController@generateContactWithoutPhoneReport');
    });
});
