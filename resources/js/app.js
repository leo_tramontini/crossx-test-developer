import Vue from 'vue'
window.Vue = Vue;
import VueRouter from 'vue-router'
Vue.use(VueRouter);

import auth from './auth'
import App from './components/App.vue'
import Dashboard from './components/Dashboard.vue'
import Login from './components/Login.vue'
import ListContact from './components/ListContact.vue';
import UpdateUser from './components/UpdateUser.vue';
import CreateContact from "./components/CreateContact.vue";
import DeleteContact from './components/DeleteContact.vue';
import UpdateContact from './components/UpdateContact.vue';
import ReportContactWithPhone from './components/ReportContactWithPhone.vue';
import ReportContactWithoutPhone from './components/ReportContactWithoutPhone.vue';

var _ = require('lodash');
import store from "./vuex/store";

function requireAuth (to, from, next) {
    if (!auth.loggedIn()) {
        next({
            path: '/login',
            query: { redirect: to.fullPath }
        })
    } else {
        next();
    }
}

const router = new VueRouter({
    mode: 'history',
    base: __dirname,
    routes: [
        {
            path: '/dashboard',
            component: Dashboard,
            beforeEnter: requireAuth,
            children: [
                {
                    path:'list-contact',
                    component: ListContact,
                    children: [
                        {
                            path:'create',
                            component: CreateContact,
                            meta: {
                                showModal: true
                            }
                        },
                        {
                            path:'delete',
                            component: DeleteContact,
                            meta: {
                                showModal: true
                            }
                        },
                        {
                            path:'update',
                            component: UpdateContact,
                            meta: {
                                showModal: true
                            }
                        },
                    ]
                },
                {
                    path:'report-contact-with-phone',
                    component: ReportContactWithPhone
                },
                {
                    path:'update-user',
                    component: UpdateUser,
                    meta: {
                        showDashModal: true
                    }
                },
                {
                    path:'report-contact-without-phone',
                    component: ReportContactWithoutPhone
                }
            ]
        },
        { path: '/login', component: Login },
        { path: '/logout',
            beforeEnter (to, from, next) {
                auth.logout();
                next('/login');
            }
        }
    ]
});

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    store,
    // replace the content of <div id="app"></div> with App
    render: h => h(App)
});
