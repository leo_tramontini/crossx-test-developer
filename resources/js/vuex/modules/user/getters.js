export default {
    userToken: state => {
        return state.token;
    },

    user: state => {
        return state.user;
    }
}
