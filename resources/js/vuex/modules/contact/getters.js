export default {
    contact: state => {
        return state.contact;
    },

    selectToDelete: state => {
        return state.selectToDelete;
    },

    contactToUpdateId: state => {
        return state.contactToUpdateId;
    },

    contactToUpdateIndex: state => {
        return state.contactToUpdateIndex;
    }
}
