export default {
    changeContact(context, payload) {
        context.commit("CHANGE_CONTACT", payload);
    },

    changeSelectToDelete(context, payload) {
        context.commit("CHANGE_SELECT_TO_DELETE", payload);
    },

    changeContactToUpdateId(context, payload) {
        context.commit("CHANGE_CONTACT_TO_UPDATE_ID", payload);
    },

    changeContactToUpdateIndex(context, payload) {
        context.commit("CHANGE_CONTACT_TO_UPDATE_INDEX", payload);
    }
}
