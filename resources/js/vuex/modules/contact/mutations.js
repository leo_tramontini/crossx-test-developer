export default {
    CHANGE_CONTACT (state, payload) {
        state.contact = payload;
    },

    CHANGE_SELECT_TO_DELETE (state, payload) {
        state.selectToDelete = payload;
    },

    CHANGE_CONTACT_TO_UPDATE_ID (state, payload) {
        state.contactToUpdateId = payload;
    },

    CHANGE_CONTACT_TO_UPDATE_INDEX (state, payload) {
        state.contactToUpdateIndex = payload;
    }
}
